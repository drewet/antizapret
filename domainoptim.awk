# Skipping empty strings
(!$1) {next}

# Skipping IP addresses
(/([0-9]{1,3}\.){3}[0-9]{1,3}/) {next}

# Removing leading "www."
{gsub(/^www\./, "", $1)}

# Extracting base domain name
{
 if (/\.(ru|co|cu|com|info|net|org|gov|edu|int|mil|biz|pp|ne|msk|spb|nnov|od|in|ho|cc|dn|i|tut|v|dp|sl)\.[^.]+$/)
  {$1 = gensub(/(.+)\.([^.]+\.[^.]+\.[^.]+$)/, "\\2", "")}
 else
  {$1 = gensub(/(.+)\.([^.]+\.[^.]+$)/, "\\2", "")}
}

# Sorting domains
/^[a-d]/ {d_ad[$1] = $1}
/^[e-h]/ {d_eh[$1] = $1}
/^[i-l]/ {d_il[$1] = $1}
/^[m-p]/ {d_mp[$1] = $1}
/^[q-t]/ {d_qt[$1] = $1}
/^[u-z]/ {d_uz[$1] = $1}

/^[^a-z]/ {d_other[$1] = $1}


function printarray(arrname, arr) {
    print arrname, "= ["
    for (i in arr) {
        print "\"" i "\","
    }
    print "];"
    print ""
}

# Final function
END {
    printarray("d_ad", d_ad)
    printarray("d_eh", d_eh)
    printarray("d_il", d_il)
    printarray("d_mp", d_mp)
    printarray("d_qt", d_qt)
    printarray("d_uz", d_uz)

    printarray("d_other", d_other)
}

