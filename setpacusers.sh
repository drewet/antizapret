#!/bin/bash

# Add IP addresses to ipset 'pacusers4' and 'pacusers6' table
echo "Flushing ipset rules"
ipset destroy pacusers4
ipset flush pacusers4
ipset destroy pacusers6
ipset flush pacusers6

# Creating it again
ipset create pacusers4 hash:ip maxelem 262144
ipset create pacusers6 hash:ip family inet6 maxelem 131072

echo "Adding pacusers to ipset"

ipset restore < <(awk '(/\/proxy\.pac/ && $1 !~ /\:/){print "add pacusers4",$1}' /var/log/nginx/az.log /var/log/nginx/az.log.? | sort -u)
ipset restore < <(awk '(/\/proxy\.pac/ && $1 ~ /\:/){print "add pacusers6",$1}' /var/log/nginx/az.log /var/log/nginx/az.log.? | sort -u)
echo "Done!"
