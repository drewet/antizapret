#!/bin/bash
source ./config.sh

# Resolve A records from hostnames and build IP-Hostname file (hosts)
parallel -j 3 -n 128 --progress -a hostlist.txt "dig a +noadditional +noauthority +nocmd +nocomments +time=2 +tries=1 {} @${DNS1} | awk '/^$/ {next} /^;/ {next} \$5 ~ /\.$/ {next}  {print \$5,\$1}'" > resolved-hostnames-ipdomain.txt
sort -u < resolved-hostnames-ipdomain.txt > resolved-hostnames-ipdomain_.txt
rm resolved-hostnames-ipdomain.txt
mv resolved-hostnames-ipdomain_.txt resolved-hostnames-ipdomain.txt

# Resolve IPv6
parallel -j 3 -n 128 --progress -a hostlist.txt "dig aaaa +noadditional +noauthority +nocmd +nocomments +time=2 +tries=1 {} @${DNS1} | awk '/^$/ {next} /^;/ {next} \$5 ~ /\.$/ {next}  {print \$5,\$1}'" > resolved-hostnames-ipdomain-ipv6.txt
sort -u < resolved-hostnames-ipdomain-ipv6.txt > resolved-hostnames-ipdomain-ipv6_.txt
rm resolved-hostnames-ipdomain-ipv6.txt
mv resolved-hostnames-ipdomain-ipv6_.txt resolved-hostnames-ipdomain-ipv6.txt

# Build IP only file from resolved previously hostnames
awk '{print $1}' resolved-hostnames-ipdomain.txt > resolved-hostnames-t.txt
awk '{print $1}' resolved-hostnames-ipdomain-ipv6.txt > resolved-hostnames-ipv6-t.txt

# Resolve IP addresses only from another DNS server
if [ "$DNS2" ];
then
parallel -j 3 -n 128 --progress -a hostlist.txt "dig a +short +time=2 +tries=1 {} @${DNS2}" >> resolved-hostnames-t.txt
fi;

# Deal with ignored hosts
echo -n > ./ignorehosts-resolved.txt
parallel -j 1 -a ignorehosts.txt 'echo Resolving {}; dig a +short {} | egrep "^([0-9]{1,3}\.){3}[0-9]" >> ./ignorehosts-resolved.txt'

echo -n > ./ignorehosts-resolved-ipv6.txt
parallel -j 1 -a ignorehosts.txt "echo Resolving {}; dig aaaa +noadditional +noauthority +nocmd +nocomments {} | awk '/^$/ {next} /^;/ {next} \$5 ~ /\.$/ {next}  {print \$5}'" >> ./ignorehosts-resolved-ipv6.txt

# Build final IP list
cat resolved-hostnames-t.txt iplist.txt > resolved-hostnames-f.txt
cat resolved-hostnames-f.txt | egrep '^([0-9]{1,3}\.){3}([0-9]){1,3}$' | sort -u | grep -F -v -f ignoreips.txt | grep -F -v -f ignorehosts-resolved.txt > iplist.txt

cat resolved-hostnames-ipv6-t.txt | sort -u | awk '/^$/ {next} {print}' | grep -F -v -f ignorehosts-resolved-ipv6.txt | grep -F -v -f ignoreips.txt > iplist-ipv6.txt

echo -n "Total IPs after resolving: "
cat iplist.txt | wc -l
