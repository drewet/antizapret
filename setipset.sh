#!/bin/bash

# Add IP addresses to ipset 'zapret' table
echo "Flushing ipset rules"
ipset destroy zapret
ipset flush zapret
ipset destroy zapret6
ipset flush zapret6

# Creating it again
ipset create zapret hash:ip maxelem 262144
ipset create zapret6 hash:ip family inet6 maxelem 131072

echo > ipset-save.txt

echo "Reading iplist.txt"
while read line
do
	#iptables -A zapret -d "$line" -j ACCEPT
	echo add zapret "$line" >> ipset-save.txt
done < iplist.txt

while read line
do
	#iptables -A zapret -d "$line" -j ACCEPT
	echo add zapret6 "$line" >> ipset-save.txt
done < iplist-ipv6.txt

# "Restore" iptables with filled zapret table
ipset restore < ipset-save.txt
echo "Done!"
