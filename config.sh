#!/bin/sh

# DNS server IP address to generate dnsmasq config
DNSPROXYIP='107.150.11.192'

# HTTPS (TLS) proxy address
PACHTTPSHOST='proxy.antizapret.prostovpn.org:3143'

# Usual proxy address
PACPROXYHOST='proxy.antizapret.prostovpn.org:3128'

#Primary DNS which is used to resolve address
DNS1='8.8.8.8'

# Secondary DNS for resolving
DNS2='194.28.28.3'
